# Probe

Probe provides a simple framework for CSV::Table and CSV::Row linting using custom rules.
Probe extens CSV::Table and CSV::Row with a `lint(rules, opts)` method.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'csv-probe'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install csv-probe

## Usage

Example how to use Probe

```ruby
  # load csv-probe gem
  require 'csv/probe'

  # load CSV into CSV::Table
  csv_table = CSV.parse(<<~ROWS, headers: true)
    col1,col2,col3,col4,col5,col6,col7
    0,NU,customer,1,02-12-2021,FO;FO;RU,eagle|hawk
    0,PA,customer,2,03-12-2021,SHA;AN;RU,badger|ant
    0,TE,guest,1000,06-11-2021,RU,spider|racoon|ant
  ROWS

  # define linting rules
  checks = [Probe::ColumnIsEqualTo.new("col1", "0"),
            Probe::ColumnMatchesRegEx.new("col2", /^(NU|PA|TE)$/),
            Probe::ColumnIsOneOf.new("col3", %w[customer guest]),
            Probe::ColumnMeetsCondition.new("col4", lambda { |val, _opts|
              !Integer(val, exception: false).nil?
            }, "Not an Integer"),
            Probe::ColumnIsDate.new("col5", "%d-%m-%Y"),
            Probe::ColumnIsListWithDomain.new("col6", %w[FO RU SHA AN], ";"),
            Probe::ColumnIsSetWithDomain.new("col7", %w[eagle hawk badger spider racoon ant], "|")]

  # lint table
  csv_table.lint(checks)

  # lint single row of table
  csv_table[0].lint(checks)
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

Rubocop requires `libssl.so.1.1`
* Install package `openssl-1.1`

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/homebase-dev/csv-probe.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
