# frozen_string_literal: true

module Probe
  class Error < StandardError; end

  class ChecksInvalidTypeError < Error; end

  class LintingError < Error; end

  # Linting error in CSV::Row, if the rule applies to multiple columns of a row
  class RowError < LintingError
    def initialize(msg = "CheckRowError")
      super
    end
  end

  # Linting warning in CSV::Row, if the rule applies to multiple columns of a row
  class RowWarning < LintingError
    def initialize(msg = "CheckRowWarning")
      super
    end
  end

  # Linting error in CSV::Column, if the rule applies to a single column of a row
  class ColumnError < LintingError
    def initialize(msg = "CheckColumnError")
      super
    end
  end

  # Linting warning in CSV::Column, if the rule applies to a single column of a row
  class ColumnWarning < LintingError
    def initialize(msg = "CheckColumnWarning")
      super
    end
  end

  # Abstract class of a CSV::Row check
  class RowMeetsCondition
    attr_accessor :ok_condition_fn, :fail_msg, :severity, :pre_checks

    def initialize(ok_condition_fn, fail_msg)
      @pre_checks = []
      @ok_condition_fn = ok_condition_fn
      @fail_msg = fail_msg
      @severity = "ERROR"
      @error_classes = {
        "ERROR" => RowError,
        "WARNING" => RowWarning
      }
    end

    def evaluate_pre_checks(row, opts)
      @pre_checks.each do |c|
        c.evaluate(row, opts)
      end
    end

    def evaluate(row, opts = {})
      evaluate_pre_checks(row, opts)
      raise @error_classes.fetch(@severity), error_msg(row, opts) unless @ok_condition_fn.call(row, opts)
    end

    def error_msg(row, opts)
      row_location = source_row_location(row, opts)
      row_str = source_row(row, opts) # [#{self.class}] verbose only?
      err_msg = "#{row_location} [#{@severity}] #{render_pretty_fail_msg(row, opts)}\n"
      if opts[:verbose]
        err_msg = "#{err_msg}#{self.class.name.split("::").last}\n"
        err_msg = "#{err_msg}#{@error_classes.fetch(@severity).name.split("::").last}\n"
      end
      "#{err_msg}#{row_str}\n"
    end

    def render_fail_msg(row, opts)
      return @fail_msg.call(row, opts) if @fail_msg.is_a?(Proc)

      @fail_msg
    end

    def render_pretty_fail_msg(row, opts)
      render_fail_msg(row, opts)
    end

    def source_row_location(_row, opts)
      "#{opts[:fpath]}:#{opts[:lineno]}"
    end

    def source_row(row, opts = {})
      return report_columns_hash(row, opts) if opts[:only_report_columns]

      if opts[:source_row_renderer]
        return row.inspect if opts[:source_row_renderer] == "inspect"
        return "#{Terminal::Table.new rows: row.to_a}\n" if opts[:source_row_renderer] == "tabular"
        return "#{Terminal::Table.new rows: row.to_a.transpose}\n" if opts[:source_row_renderer] == "tabular_transposed"
      end
      row
    end

    # def row_colval(row, varname, opts)
    #   return row[varname] if varname.is_a? Integer
    #   return row.fetch(varname)
    # end

    def report_columns_hash(row, opts)
      h = {}
      opts[:only_report_columns].each { |varname| h[varname] = row.fetch(varname) }
      h
    end
  end

  # Abstract class of a CSV::Row single field (= column) check
  class ColumnMeetsCondition < RowMeetsCondition
    attr_accessor :varname, :ok_condition_fn, :fail_msg

    def initialize(varname, ok_condition_fn, fail_msg)
      super(ok_condition_fn, fail_msg)
      @varname = varname
      @error_classes = {
        "ERROR" => ColumnError,
        "WARNING" => ColumnWarning
      }
    end

    def render_pretty_fail_msg(row, opts)
      "Unexpected value:#{row.fetch(@varname).inspect} for column:#{@varname.inspect}, #{render_fail_msg(row, opts)}"
    end

    def evaluate(row, opts = {})
      evaluate_pre_checks(row, opts)

      return if @ok_condition_fn.call(row.fetch(@varname), opts)

      raise @error_classes.fetch(@severity), error_msg(row, opts)

      # unless @ok_condition_fn.call(row.fetch(@varname), opts)
      #  raise @error_classes.fetch(@severity), error_msg(row, opts)
      # end
    end
  end

  # Check if a column value is equal to a provided value (Pay attention to types!)
  class ColumnIsEqualTo < ColumnMeetsCondition
    def initialize(varname, expected_val, _placeholder = nil)
      super(varname, nil, nil)
      @ok_condition_fn = ->(val, _cfg) { val == expected_val }
      @fail_msg = "expected to be #{expected_val.inspect}"
    end
  end

  # Check if a column value is one of a list of provided value
  class ColumnIsOneOf < ColumnMeetsCondition
    def initialize(varname, expected_vals_arr, _placeholder = nil)
      super(varname, nil, nil)
      @ok_condition_fn = ->(val, _cfg) { expected_vals_arr.include?(val) }
      @fail_msg = "expected to be one of #{expected_vals_arr.inspect}"
    end
  end

  # Check if a column value is a date with a given format
  class ColumnIsDate < ColumnMeetsCondition
    def initialize(varname, expected_date_format, _placeholder = nil)
      super(varname, nil, nil)
      @ok_condition_fn = lambda { |val, _cfg|
        success = true
        begin
          Date.strptime(val, expected_date_format)
        rescue Date::Error
          success = false
        end
        success
      }
      @fail_msg = "expected date with format #{expected_date_format.inspect}"
    end
  end

  # class ColumnIsInteger < ColumnMeetsCondition
  #   def initialize(varname, placeholder1=nil, placeholder=nil)
  #     super(varname, nil, nil)
  #     @ok_condition_fn = ->(val, cfg){ val.is_a? Integer }
  #       # TODO: ATTENTION 12abc -> integer, also  1.1 -> 1 ! better do it over regex?
  #     @fail_msg = "expected to be an Integer"
  #   end
  # end

  # Check if a column value is a date with a given format
  class ColumnMatchesRegEx < ColumnMeetsCondition
    def initialize(varname, expected_regex_pattern, _placeholder = nil)
      super(varname, nil, nil)
      @ok_condition_fn = ->(val, _cfg) { val.to_s =~ expected_regex_pattern }
      @fail_msg = "expected to match regex pattern #{expected_regex_pattern.inspect}"
    end
  end

  # Check if a column value is a date with a given format
  class ColumnNotMatchesRegEx < ColumnMeetsCondition
    def initialize(varname, expected_regex_pattern, _placeholder = nil)
      super(varname, nil, nil)
      @ok_condition_fn = ->(val, _cfg) { val.to_s !~ expected_regex_pattern }
      @fail_msg = "expected to NOT match regex pattern #{expected_regex_pattern.inspect}"
    end
  end

  # rubocop:disable Metrics/AbcSize
  # Check if a tokenized column value is a list of given values
  class ColumnIsListWithDomain < ColumnMeetsCondition
    def initialize(varname, expected_items_arr, separator, _placeholder = nil)
      super(varname, nil, nil)
      @ok_condition_fn = lambda { |val, _cfg|
        expected_items_arr.map!(&:to_s) # turn nil -> ""

        items = val.to_s.split(separator, -1)
        return true if items.empty? && expected_items_arr.include?(nil.to_s) # empty str allowed

        return false if items.empty?

        items.all? { |e| expected_items_arr.include?(e) }
      }
      @fail_msg = lambda { |row, _opts|
        items = row.fetch(@varname).to_s.split(separator, -1)
        diff_items = items - expected_items_arr
        "expected that tokenized items of value #{items.inspect} are a subset of " \
          "#{expected_items_arr.inspect}, but items #{diff_items.inspect} are not"
      }
    end
  end

  # Check if a tokenized column value is a list of given values
  class ColumnIsSet < ColumnMeetsCondition
    def initialize(varname, separator, _placeholder = nil)
      super(varname, nil, nil)
      @ok_condition_fn = lambda { |val, _cfg|
        return true if val.to_s == ""

        items = val.to_s.split(separator, -1)
        all_uniq = (items.size == items.uniq.size)
        all_uniq
      }
      @fail_msg = lambda { |row, _opts|
        items = row.fetch(@varname).to_s.split(separator, -1)
        non_uniq_items = items.detect { |e| items.count(e) > 1 }
        "expected that items of tokenized value #{items.inspect} are uniqe, but items #{non_uniq_items.inspect} are not"
      }
    end
  end

  # Check if a tokenized column value is a set of given values (no duplicates)
  class ColumnIsSetWithDomain < ColumnMeetsCondition
    def initialize(varname, expected_items_arr, separator, _placeholder = nil)
      super(varname, nil, nil)
      @pre_checks << ColumnIsSet.new(varname, separator)
      @ok_condition_fn = lambda { |val, _cfg|
        expected_items_arr.map!(&:to_s) # turn nil -> ""

        items = val.to_s.split(separator, -1)
        return true if items.empty? && expected_items_arr.include?(nil.to_s) # empty str allowed

        return false if items.empty?

        all_valid = items.all? { |i| expected_items_arr.include?(i) }
        all_valid
      }
      @fail_msg = lambda { |row, _opts|
        items = row.fetch(@varname).to_s.split(separator, -1)
        "expected that items of tokenized value #{items.inspect} are a subset of #{expected_items_arr.inspect}"
      }
      # @fail_msg = lambda {|row, opts|
      # "Unexpected value:#{row.fetch(@varname).inspect} for column:#{@varname.inspect},
      #  expected that items of tokenized value #{row.fetch(@varname).split(separator)}
      #  are uniqe and a subset of #{expected_items_arr.inspect}" }
    end
  end
  # rubocop:enable Metrics/AbcSize

  # TODO: nice to have
  # * if_second_not_empty_then_first_yes_check(varname1, varname2)
  # * if_first_eyes_then_second_not_empty_check(varname1, varname2)
  # * if_first_yes_then_others_too_checks(varname1, varname_arr)
  # * if_first_NC2_then_others_not_yes_checks(varname1, varname_arr)
  # * if_first_yes_then_also_second_check(varname1, varname2)
  # * warn_if_first_yes_then_others_not_empty_checks(main_varname, sub_varnames_arr)
  # * is_comment_of_size(main_varname, char_len)
end
