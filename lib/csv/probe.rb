# frozen_string_literal: true

require "csv" # TODO: should i require it here?
require "terminal-table"
require_relative "probe/version"
require_relative "probe/checks"

# Probe provides methods for linting a:
# * CSV::Table
# * CSV::Row
#
# The linting methods:
# * lint_rows takes following arguments
#   * csv_rows: CSV::Table
#   * checks: Array of Probe Checks and
#   * options
# * lint_row takes following arguments
#   * csv_row: CSV::Row
#   * checks: Array of Probe Checks and
#   * optsions
module Probe
  def self.lint_rows(csv_rows, checks, opts = {})
    lineno_start = opts[:headers] ? 2 : 1
    csv_rows.each.with_index(lineno_start) do |row, lineno|
      opts[:lineno] = lineno
      lint_row(row, checks, opts)
    end
  end

  def self.lint_row(csv_row, checks, opts = {})
    unless checks.is_a? Array
      raise ChecksInvalidTypeError, "checks are expected to be of type error, but are of type '#{checks.class}'"
    end

    return if checks.empty? # nothing to do if there are no checks

    unless csv_row.is_a? CSV::Row
      raise Error "lint_row(csv_row,...), csv_row is not of type CSV::Row, but of type '#{csv_row.class}'"
    end

    checks.each do |check|
      check.evaluate(csv_row, opts)
    rescue LintingError => e
      raise e unless opts[:exception] == false

      puts e.message # if exception oppressed, write error out on stdout
    end
  end
end

# Extend CSV::Table with .lint(...) method
class CSV::Table # rubocop:disable Style/ClassAndModuleChildren
  def lint(checks, opts = {})
    opts[:headers] = true unless opts.key?(:headers) # CSV::Table has always headers, hence set :headers = true
    Probe.lint_rows(self, checks, opts)
  end
end

# Extend CSV::Row with .lint(...) method
class CSV::Row # rubocop:disable Style/ClassAndModuleChildren
  def lint(checks, opts = {})
    Probe.lint_row(self, checks, opts)
  end
end
