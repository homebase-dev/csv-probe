# frozen_string_literal: true

RSpec.describe Probe do # rubocop:disable Metrics/BlockLength
  it "has a version number" do
    expect(Probe::VERSION).not_to be nil
  end

  it "passes CSV::ROW and CSV::Table checks" do
    csv_table = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3,col4,col5,col6,col7
      0,NU,customer,1,02-12-2021,FO;FO;RU,eagle|hawk
      0,PA,customer,2,03-12-2021,SHA;AN;RU,badger|ant
      0,TE,guest,1000,06-11-2021,RU,spider|racoon|ant
    ROWS
    checks = [Probe::ColumnIsEqualTo.new("col1", "0"),
              Probe::ColumnMatchesRegEx.new("col2", /^(NU|PA|TE)$/),
              Probe::ColumnIsOneOf.new("col3", %w[customer guest]),
              Probe::ColumnMeetsCondition.new("col4", lambda { |val, _opts|
                !Integer(val, exception: false).nil?
              }, "Not an Integer"),
              Probe::ColumnIsDate.new("col5", "%d-%m-%Y"),
              Probe::ColumnIsListWithDomain.new("col6", %w[FO RU SHA AN], ";"),
              Probe::ColumnIsSetWithDomain.new("col7", %w[eagle hawk badger spider racoon ant], "|")]
    expect do
      csv_table[0].lint(checks) # CSV::ROW
      csv_table.lint(checks)    # CSV::Table
    end.not_to raise_error
  end

  it "matches ColumnMatchesRegEx checks" do
    data = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3
      test,18a,aaa
    ROWS
    expect do
      data.lint([Probe::ColumnMatchesRegEx.new("col1", /^tes/),
                 Probe::ColumnMatchesRegEx.new("col1", /^test$/),
                 Probe::ColumnMatchesRegEx.new("col2", /^\d+a$/),
                 Probe::ColumnMatchesRegEx.new("col3", /^[a-z]{3}$/)])
    end.not_to raise_error
  end

  it "fails ColumnMatchesRegEx checks" do
    data = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3
      test,18a,aaa
    ROWS
    expect { data.lint([Probe::ColumnMatchesRegEx.new("col1", /^ts/)]) }.to raise_error(Probe::ColumnError)
    expect { data.lint([Probe::ColumnMatchesRegEx.new("col2", /^\da/)]) }.to raise_error(Probe::ColumnError)
    expect { data.lint([Probe::ColumnMatchesRegEx.new("col3", /^[a-z]{2}$/)]) }.to raise_error(Probe::ColumnError)
  end

  it "matches ColumnNotMatchesRegEx checks" do
    data = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3
      test,18a,aaa
    ROWS
    expect do
      data.lint([Probe::ColumnNotMatchesRegEx.new("col1", /^tes1/),
                 Probe::ColumnNotMatchesRegEx.new("col1", /^test1$/),
                 Probe::ColumnNotMatchesRegEx.new("col2", /^\d+$/),
                 Probe::ColumnNotMatchesRegEx.new("col3", /^[a-z]{2}$/)])
    end.not_to raise_error
  end

  it "fails ColumnNotMatchesRegEx checks" do
    data = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3
      test,18a,aaa
    ROWS
    expect { data.lint([Probe::ColumnNotMatchesRegEx.new("col1", /^test/)]) }.to raise_error(Probe::ColumnError)
    expect { data.lint([Probe::ColumnNotMatchesRegEx.new("col2", /^\d+a/)]) }.to raise_error(Probe::ColumnError)
    expect { data.lint([Probe::ColumnNotMatchesRegEx.new("col3", /^[a-z]{3}$/)]) }.to raise_error(Probe::ColumnError)
  end

  it "fails ColumnIsEqualTo checks" do
    data = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3
      12,18a,
    ROWS
    expect { data.lint([Probe::ColumnIsEqualTo.new("col2", "18")]) }.to raise_error(Probe::ColumnError)
    expect { data.lint([Probe::ColumnIsEqualTo.new("col3", "18")]) }.to raise_error(Probe::ColumnError)
  end

  it "passes ColumnIsEqualTo checks" do
    data = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3
      12,,xyz
    ROWS
    checks = [Probe::ColumnIsEqualTo.new("col1", 12),
              Probe::ColumnIsEqualTo.new("col2", ""),
              Probe::ColumnIsEqualTo.new("col3", nil),
              Probe::ColumnIsEqualTo.new("col4", "xyz")]
    expect { data.lint(checks) }.to raise_error(Probe::ColumnError)
  end

  it "passes ColumnIsOneOf checks" do
    data = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3
      12,1,x
      12,2,x
    ROWS
    checks = [Probe::ColumnIsOneOf.new("col2", %w[1 2]),
              Probe::ColumnIsOneOf.new("col2", %w[0 1 2 3]),
              Probe::ColumnIsOneOf.new("col3", %w[x])]
    expect { data.lint(checks) }.not_to raise_error
  end

  it "fails ColumnIsOneOf checks" do
    data = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3
      12,1,x
      12,2,x
    ROWS
    expect { data.lint([Probe::ColumnIsOneOf.new("col2", ["0"])]) }.to raise_error(Probe::ColumnError)
    expect { data.lint([Probe::ColumnIsOneOf.new("col2", [1, 2])]) }.to raise_error(Probe::ColumnError)
    expect { data.lint([Probe::ColumnIsOneOf.new("col2", ["a", "b", "3", 6])]) }.to raise_error(Probe::ColumnError)
    expect { data.lint([Probe::ColumnIsOneOf.new("col2", [])]) }.to raise_error(Probe::ColumnError)
  end

  it "passes ColumnIsDate checks" do
    data = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3
      12-12-2021,1,x
      01-02-2021,1,x
    ROWS
    expect { data.lint([Probe::ColumnIsDate.new("col1", "%d-%m-%Y")]) }.not_to raise_error
  end

  it "fails ColumnIsDate checks" do
    data = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3
      32-12-2021,1,x
    ROWS
    expect { data.lint([Probe::ColumnIsDate.new("col1", "%d-%m-%Y")]) }.to raise_error(Probe::ColumnError)
    data = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3
      12--2021,1,x
    ROWS
    expect { data.lint([Probe::ColumnIsDate.new("col1", "%d-%m-%Y")]) }.to raise_error(Probe::ColumnError)
    data = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3
      12+12-2021,1,x
    ROWS
    expect { data.lint([Probe::ColumnIsDate.new("col1", "%d-%m-%Y")]) }.to raise_error(Probe::ColumnError)
  end

  it "passes ColumnIsListWithDomain checks" do
    data1 = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3
      a,1;1;2;3;4,x
      b,1,x
      c,5;6;7,x
    ROWS
    expect { data1.lint([Probe::ColumnIsListWithDomain.new("col2", %w[1 2 3 4 5 6 7], ";")]) }.not_to raise_error
  end

  it "fails ColumnIsListWithDomain check because of unexpected 8 value" do
    data = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3
      a,1;2;3;4,x
      b,1,x
      c,5;6;7;8,x
    ROWS
    expect do
      data.lint([Probe::ColumnIsListWithDomain.new("col2", %w[1 2 3 4 5 6 7], ";")])
    end.to raise_error(Probe::ColumnError)
  end

  it "fails ColumnIsListWithDomain check because of unexpected nil value in" do
    data = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3
      a,aa|bb|,x
      b,cc,x
      c,|dd,x
    ROWS
    expect do
      data.lint([Probe::ColumnIsListWithDomain.new("col2", %w[aa bb cc dd], "|")])
    end.to raise_error(Probe::ColumnError)
  end

  it "fails ColumnIsListWithDomain check because of unexpected nil value as list" do
    data = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3
      a,,x
    ROWS
    expect do
      data.lint([Probe::ColumnIsListWithDomain.new("col2", %w[1 2], "|")])
    end.to raise_error(Probe::ColumnError)
  end

  it "passes ColumnIsSetWithDomain check with expected values" do
    data = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3
      a,1;2;3;4,x
      b,1,x
      c,5;6;7,x
    ROWS
    expect { data.lint([Probe::ColumnIsSetWithDomain.new("col2", %w[1 2 3 4 5 6 7], ";")]) }.not_to raise_error
  end

  it "passes ColumnIsSetWithDomain check with expected nil or empty string values" do
    data = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3
      a,1||2,x
    ROWS
    expect do
      data.lint([Probe::ColumnIsSetWithDomain.new("col2", ["1", "2", nil], "|")])
    end.not_to raise_error
    expect do
      data.lint([Probe::ColumnIsSetWithDomain.new("col2", ["1", "2", ""], "|")])
    end.not_to raise_error
  end

  it "passes ColumnIsSetWithDomain check with expected nil value as set" do
    data = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3
      a,1,x
      a,,x
      a,2,x
    ROWS
    expect { data.lint([Probe::ColumnIsSetWithDomain.new("col2", ["1", "2", nil], ";")]) }.not_to raise_error
  end

  it "fails ColumnIsSetWithDomain check because of unexpected 8 value " do
    data = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3
      a,1;2;3;4,x
      b,1,x
      c,5;6;7;8,x
    ROWS
    expect do
      data.lint([Probe::ColumnIsSetWithDomain.new("col2", %w[1 2 3 4 5 6 7], ";")])
    end.to raise_error(Probe::ColumnError)
  end

  it "fails ColumnIsSetWithDomain check because of unexpected nil value in list" do
    data = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3
      a,aa|bb|,x
    ROWS
    expect do
      data.lint([Probe::ColumnIsSetWithDomain.new("col2", %w[aa bb cc dd], "|")])
    end.to raise_error(Probe::ColumnError)
  end

  it "fails ColumnIsSetWithDomain checks because of unexpected nil value as list" do
    data = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3
      a,,x
    ROWS
    expect { data.lint([Probe::ColumnIsSetWithDomain.new("col2", %w[1 2], "|")]) }.to raise_error(Probe::ColumnError)
  end

  it "fails ColumnIsSetWithDomain checks because of unexpected duplicate value" do
    data = CSV.parse(<<~ROWS, headers: true)
      col1,col2,col3
      a,1|1|2,x
    ROWS
    expect { data.lint([Probe::ColumnIsSetWithDomain.new("col2", %w[1 2], "|")]) }.to raise_error(Probe::ColumnError)
  end
end
