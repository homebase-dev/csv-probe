## [Unreleased]

## [0.1.6] - 2022-11-11
- Add ColumnNotMatchesRegEx class
- Raise InvalidTypeError when passed checks argument is not of type array

## [0.1.5] - 2022-01-05
- Fix rubocop warnings

## [0.1.4] - 2022-01-05
- Fix `nil` handling for list + set column checks when the list or set is something like this:
  - `a,|b|,b` leading and trailing empty items or 
  - `a,,b` the whole list/set string is nil

## [0.1.3] - 2022-01-04
- Add better `nil` handling to list + set column checks. `nil` resp. `""` does no longer implicitly pass ColumnIsListWithDomain and ColumnIsSetWithDomain checks. `nil` or `""` has to be in the expected items in order to pass the check now.

## [0.1.2] - 2022-01-03
- Fix README.md

## [0.1.1] - 2022-01-02
- Fix rubocop warnings
- Improve README.md

## [0.1.0] - 2022-01-02

- Initial release
